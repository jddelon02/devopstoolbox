#!/bin/bash

##############
# Just a quick script to 
# commit all submodules
# at the end of the day
##############


for dir in *; do
	if [[ -d "${dir}" ]]; then
		# Do something with $dir...
		echo "Entering: ${dir}"
		cd "${dir}"
		gittest=$(ls .git)
		echo $gittest
		if [[ $gittest ]]; then
			echo "running git"
			git add .
			git commit -m "done for the night"
			git push
		fi
		echo "finished in this directory"	
		cd ..
	fi
done
