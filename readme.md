## Synopsis

This repo is actually just a collection of various ansible-playbooks (and one shell script folder).  The repo is actually mostly empty, since each playbook directory is a separate repository.  The one thing this repo does contain are the host/inventory/host_vars/group_vars folders, which allows each submodule to share the same host info.  This does mean, however, that none of the submodules will be able to connect to any hosts on their own.

## Code Example

The simplest way to run an ansible playbook is to go into whatever folder (they are as self-explanatory as I could make them) and type:

```bash
ansible-playbook somefile.yml
```

This will run the play.  However, it will have defaulted to run on whatever hosts were defined in the <somefile.yml>

The basic structure of this entire toolbox is:

- Root
    - inventory
    - host_vars
    - group_vars
    - git_submodule
        - role
	        - tasks
	            - main.yml
	        - defaults
	            - main.yml
	        - meta
	            - main.yml
	        - vars
	            - main.yml
	        - templates
	        	- filename.j2
    	- <role_specific_playbook.yml>
    - <master_playbook.yml>


## Motivation

Each of these ansible-playbooks have been created to perform **DevOps** functions that were previously done manually.  This ability gives **Message Agency** the technical capability to scale without needing to add more staff.

## Installation

Install [Ansible](https://www.nowhereyet.com) - (detailed in confluence... eventually...)
Clone this repo, and be sure to specify recursive so it pulls down the submodules as well.

Will need to have certain variables defined in a way that allows for Ansible to do env lookups.  This alleviates the need to store user/pass creds in Ansible.


```git
git clone --recursive git@bitbucket.org:messageagency/devopstoolbox.git
```

## API Reference

Coming Soon...

## Contributors

Any suggestions are welcome!!!

## License

No clue...
